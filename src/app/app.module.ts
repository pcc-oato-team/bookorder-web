import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListbookComponent } from './book/listbook.component';
import { AddbookComponent } from './book/addbook.component';
import { UpdatebookComponent } from './book/updatebook.component';

import { AlertModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


const routes: Routes = [];

export let InjectorInstance: Injector;

@NgModule({
  declarations: [
    AppComponent,
    ListbookComponent,
    AddbookComponent,
    UpdatebookComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonsModule.forRoot(),
    AlertModule.forRoot(),
    HttpClientModule,
    FormsModule
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
                                                                                                                              
  constructor(private injector: Injector) {
    InjectorInstance = this.injector;
}

}
