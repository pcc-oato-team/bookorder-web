import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ResponseReq } from '../models/response-req';
import { books } from '../book.model';

@Injectable({
    providedIn: 'root'
  })

  export class BookHttpService{
// call http
constructor(
  private http: HttpClient
) { }

public getBook(): Observable<any> {
  const _url = 'http://192.168.7.49:8090/bookorder-service/books';
  return this.http.get<ResponseReq<any>>(_url);
}


/*
public getBook(): Observable<any> {
  return this.http.get('192.168.7.173:8090/bookorder-service/BookService');
} */

/*
public addBook(obj: object): Observable<any> {
  return this.http.post('192.168.7.173:8090/bookorder-service/BookService', obj, { responseType: 'text' });
} */


/*
deleteBook(id){
  return this.http.delete('192.168.7.173:8090/bookorder-service/BookService'+'/'+id)
} */

/*
public updareBook(model: object): Observable<any> {
  return this.http.put<ResponseReq<any>>('192.168.7.173:8090/bookorder-service/BookService', model)
    .pipe(map(res => res));
} */



  }