import { Component, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BookHttpService } from './services/book-http.service';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent {
  title = 'เพิ่มหนังสือ';
  
  public formDetail: FormGroup;
  // @ViewChild('addbook') addbook: TemplateRef<ElementRef>;

}
