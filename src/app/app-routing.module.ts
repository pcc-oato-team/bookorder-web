import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddbookComponent } from './book/addbook.component';
import { ListbookComponent } from './book/listbook.component';
import { UpdatebookComponent } from './book/updatebook.component';


const routes: Routes = [
  {path:'', component: ListbookComponent },
  {path:'addBook', component: AddbookComponent},
  {path:'updateBook', component: UpdatebookComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
