import { Component, OnInit } from '@angular/core';
import { BookHttpService } from './services/book-http.service';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-listbook',
  templateUrl: './listbook.component.html',
  styleUrls: ['./listbook.component.css']
})

export class ListbookComponent implements OnInit {
  title = 'รายการหนังสือ';
  books = [];
  private bookid : number|string;

  constructor(
    public bookHttpService: BookHttpService,
    private route: ActivatedRoute,
    private router: Router  
  ) { }


  

  ngOnInit() {
    this.bookHttpService.getBook().subscribe(res=>{
      console.log(res);
      this.books = res; //ส่งค่ากลับมาโชว์ listbook


    });

  }

  onEdit(book: any) {
   // this.router.navigate(['/updateBook'+ book.id]);
  }

}


